# javascript_challenge

Task: Build a beer navigator and viewer that uses the BreweryDB API to match provided layout and styles.

## About

The site will load as static html in a browser and uses relative style and js files, along with CDN for layout and style.

### CSS
Prepared in an SCSS file and compiled to CSS. 

### Fonts
Uses the Lato font from Google fonts

### Bootstrap
Bootstrap CDN used to give a base for layout and structure on the page. 

### Mobile menu
The top categories stay in place and the sidebar list becomes a mobile menu that pushes out from the left on smaller screen sizes.

## How it works
1. The user lands on the page, ticks the box for the category of beer that they are interested in.
2. The styles from that category are determined and the beers of that style are fetched from the API.  
3. A list of results populates the sidebar and the first entry in the sidebar is loaded into the content.
4. User selects the result they want to view and on clicking the result, it loads in teh content, replacing the previous entry.
5. Reset clears current search to load a new selection. 

### Installation and run on Linux
All files are contained in such a way that the page should be mobile responsive and run when loading index.html in any modern web browser.




