// Capture data from BreweryDB API // api-key f54999a1cddf2b7e164365024da38910

const url = 'http://api.brewerydb.com/v2/?key=f54999a1cddf2b7e164365024da38910';
const proxy = 'https://cors-anywhere.herokuapp.com/';
// const proxy = 'https://cryptic-headland-94862.herokuapp.com/';
var styleIDArr = new Array; // style id numbers
var pagesArr = new Array; // number of pages per style
var pageNumUrls = new Array; // to hold links to pages of beer based on style id
var checkFilter;
var cancel = false;

window.onload = function(){
    // Create list of tick boxes to filter results 
    var brewCategories = document.getElementById('beerCategories')
    var categoriesUrl = proxy+'http://api.brewerydb.com/v2/categories/?key=f54999a1cddf2b7e164365024da38910';
    var catArray = new Array;
    var catNum = 1;
    var singleCatUrl;
    var catName;
    var checkbox;
    var label;
    fetch(categoriesUrl)
        .then((response) => response.json())
        .then(function(data) {
            console.log(data);
        numCats = data.data.length; //get length from array in case changes in future
        
        // get each category name and put in array, display as checkboxes (numCats -1 -> because last entry is a dummy entry returning empty string)
        for(var i = 0; i < (numCats-1); i++) {
            catNum = data.data[i].id;
            catName = data.data[i].name;
            label = document.createElement('li');
            checkbox = '<label class="checkbox-inline"><input class="inputCheck" type="checkbox" value='+catNum+' onclick="asyncClickOne()">'+catName+'</label>';
            label.innerHTML = checkbox;
            label.setAttribute('class', 'col-xs-12 col-sm-6 col-lg-4');
            document.getElementById('beerCategories').appendChild(label);
        }
    })
    .catch(err => console.log(err)); 
    
    // add classes to results to set sidenav if mobile
    if(screen.width < 768) {
        document.getElementById("beerResults").classList.add('sidenav');
        document.getElementById("openMobileNav").style.display = inline-block;
    }
    
}

// Set options for mobile navigation of results
if (screen.width < 426) {
    function openNav() {
        document.getElementById("beerResults").style.width = screen.width+"px";
        document.getElementById("bodyMain").style.marginLeft = screen.width+"px";
    }

    function closeNav() {
        document.getElementById("beerResults").style.width = "0";
        document.getElementById("bodyMain").style.marginLeft = "0";
    }
}

if (screen.width >= 426 && screen.width < 768) {
    function openNav() {
        document.getElementById("beerResults").style.width = "250px";
        document.getElementById("bodyMain").style.marginLeft = "250px";
    }

    function closeNav() {
        document.getElementById("beerResults").style.width = "0";
        document.getElementById("bodyMain").style.marginLeft = "0";
    }
}


// Get all styles of beers by category in checkboxes & create an array of page links to trigger on click
async function asyncClickOne() {
    
    checkFilter = document.querySelector('.inputCheck:checked').value;
    var stylesUrl = proxy+'http://api.brewerydb.com/v2/styles/?key=f54999a1cddf2b7e164365024da38910';
    
    const styleReq = async() => {
        const loader = await document.createElement('div');
        loader.setAttribute("id", "loader");
        await document.body.appendChild(loader);
        const styleResponse = await fetch(stylesUrl);
        const styleJSON = await styleResponse.json();
        var lengArr = await styleJSON.data.length;
        
        for(let i = 0; i < lengArr; i++) {
            if(styleJSON.data[i].categoryId == checkFilter) {
                await styleIDArr.push(i+1);
                const styleIdUrl = await fetch (proxy+'http://api.brewerydb.com/v2/beers?styleId='+(i+1)+'&key=f54999a1cddf2b7e164365024da38910');
                console.log(styleIdUrl);
                const styleResponseJson = await styleIdUrl.json();
                const numPages = await styleResponseJson.numberOfPages;
                await pagesArr.push(numPages);
            }
        }
        await document.body.removeChild(loader);
        
        const buttons = '<button class="btn btn-default" onclick="reset()">Reset</button>';
        const buttonWrap = await document.getElementById("resultsClearButtons");
        buttonWrap.innerHTML = await buttons;
    }
    await styleReq();
    console.log(pagesArr);
    
    // Get beers by page using the stylesIds captured
    const getBeers = async() => {
        if (screen.width < 768) {
            openNav();
        }
    
        var styleID;
        var numPages;
        var pageTotal;
        for(let k = 0; k < pagesArr.length; k++ ) {
            // get style id to use
            const styleID = await styleIDArr[k];
            console.log(styleID);
            
            // create links to correct number of pages of that style
            const pageTotal = await pagesArr[k];
            for (let j = 0; j < pageTotal; j++) {
                const pagesUrl = await proxy+'http://api.brewerydb.com/v2/beers?styleId='+styleID+'&p='+(j+1)+'&key=f54999a1cddf2b7e164365024da38910';
                await pageNumUrls.push(pagesUrl);    
            }
        }
        console.log(pageNumUrls);
    
        // get the beer data needed for the sidebar
        var count = 1; // increment to create unique ids for sidebar elements    
    
        // load first page and set pagination in first instance (to speed page load)
        for (let index = 0; index < 1; index++) {
            const beersUrl = await pageNumUrls[index];
            const response = await fetch(beersUrl);
            const json = await response.json();
            const endArr = await json.data.length;
            
            // for each entry in each page get data to build sidebar
            for (let m = 0; m < endArr; m++) {
                const result = async function() {
                    const beerRef = await json.data[m].id;
                    const beerName = await json.data[m].name;
                    const labelName = await document.createElement('li');
                    const contentLink = await '<a id="link-'+(count++)+'" class="content-load" data-link="'+proxy+'http://api.brewerydb.com/v2/beer/'+beerRef+'?key=f54999a1cddf2b7e164365024da38910" onclick="loadContent(this)">'+beerName+'</a>';
                    labelName.innerHTML = await contentLink;
                    await document.getElementById('beerResults').appendChild(labelName); 
                }
                result();
            }
        }    
        
        const loadRemaining = async function() {
            for (let index = 1; index < pageNumUrls.length; index++) {
                const beersUrl = await pageNumUrls[index];
                const response = await fetch(beersUrl);
                const json = await response.json();
                const endArr = await json.data.length;
                
                // for each entry in each page get data to build sidebar
                for (let m = 0; m < endArr; m++) {
                    const result = async function() {
                        const beerRef = await json.data[m].id;
                        const beerName = await json.data[m].name;
                        const labelName = await document.createElement('li');
                        const contentLink = await '<a id="link-'+(count++)+'" class="content-load" data-link="'+proxy+'http://api.brewerydb.com/v2/beer/'+beerRef+'?key=f54999a1cddf2b7e164365024da38910" onclick="loadContent(this)">'+beerName+'</a>';
                        labelName.innerHTML = await contentLink;
                        await document.getElementById('beerResults').appendChild(labelName); 
                    }
                    result();
                }
            }    
        }
        loadRemaining();
    } // end getBeers
    await getBeers();
    
    // get first in list and load on page to replace default or placeholder content
    setTimeout(function() {
        linkToLoad = document.getElementById("link-1");
        loadContent(linkToLoad);
    }, 1000);
}


// Reset to get fresh set of results
function reset(){
    // clear selected checkboxes
    var container = document.getElementById("beerCategories");
    var content = container.innerHTML;
    container.innerHTML= content; 
    
    // clear sidebar with repeat if long list of results
    var clear = function() {
        var clearResults = document.getElementById("beerResults");
        while(clearResults.firstChild) {
            clearResults.removeChild(clearResults.firstChild);
        }
    }
    clear();
    var repeatNum = 0;
    var repeat = function() {
        setInterval(function() {
            if (repeatNum < 2) {
                clear();
                repeatNum++;
            } else {
                clearInterval(repeat);
            }
        },500);
    }
    repeat();

    var container = document.getElementById("beerResults");
    var content2 = '<h6 class="upper hide-mobile">Results</h6>';
    container.innerHTML= content2; 

    // clear content
    var container3 = document.getElementById("image");
    var content3 = '<img id="beerImage" src="images/placeholder_image.jpg" alt="Image for selected beer" class="default selected-image">';
    container3.innerHTML= content3;

    var container4 = document.getElementById("content-title");
    var content4 = "Beer Name";
    container4.innerHTML= content4; 
    
    var resetArr = ["resultsClearButtons", "beer-description", "abv-content", "ibu-content", "srm", "og", "beer-style", "brewery-name"];
    resetArr.forEach(element => {
        var container = document.getElementById(element);
        container.innerHTML = "";
    });

    // reset global checked filter  // empty urls from any previous queries
    checkFilter = "";
    styleIDArr = [];
    pagesArr = [];
    pageNumUrls = [];
}


// Load relevant beer content in main section on page when beer name clicked in sidebar
var contentUrl;
var getID;
var nullString = "N/A";

function loadContent(e) {
    if (screen.width < 768) {
        closeNav();
    }

    var activeLi = document.querySelectorAll("li");
    [].forEach.call(activeLi, function(el) {
        el.classList.remove("active");
    })
    getID = e.id;
    contentUrl = document.getElementById(getID).getAttribute("data-link");
    var idStyle = document.getElementById(getID).parentNode.classList.add("active");
    // Get brewery of specific beer
    getID = contentUrl.substring(69,75);
    var breweryUrl = proxy+'http://api.brewerydb.com/v2/beer/'+getID+'/breweries?key=f54999a1cddf2b7e164365024da38910';    
    var brewery;
    fetch(breweryUrl)
    .then((response) => response.json())
    .then(function(data) {
        if (data.data[0].name) {
            brewery = data.data[0].name;
        } else {
            brewery = nullString;
        }
        document.getElementById('brewery-name').innerHTML = brewery;            
    })
    .catch(err => console.log(err));
    
    // Get details to add to content section about beer and ABV/IBU for sliders at top
    var contentUrl = proxy+'http://api.brewerydb.com/v2/beer/'+getID+'?key=f54999a1cddf2b7e164365024da38910';
    var title;
    var description;
    var abv;
    var abvMin;
    var abvMax;
    var ibuMin;
    var ibuMax;
    var srm;
    var og;
    var imageDetail;
    var contentStyle;
    fetch(contentUrl)
    .then((response) => response.json())
    .then(function(data) {
        console.log(data);
        // TO DO: (If time) Create a function to pass all values through to remove repetitive if/else statements
        title = data.data.name;
        if (data.data.description) {
            description = data.data.description;
        } else {
            abv = nullString;
        }
        if (data.data.abv) {
            abv = data.data.abv;
        } else {
            abv = nullString;
        }
        abvMin = data.data.style.abvMin;
        abvMax = data.data.style.abvMax;
        if (data.data.ibuMin) {
            ibuMin = data.data.ibuMin;
        } else {
            ibuMin = nullString;
        }
        ibuMax = data.data.style.ibuMax;
        if (data.data.style.srmMax) {
            srm = data.data.srmMax;
        } 
        else {
            srm = nullString;
        }
        if (data.data.og) {
            og = data.data.og;
        } else {
            og = nullString;
        }
        if (data.data.og) {
            contentStyle = data.data.style.name;
        } else {
            contentStyle = nullString;
        }        
        if (data.data.labels) {
            imageDetail = data.data.labels.medium;
        } else {
            imageDetail = "images/placeholder_image.jpg";
        }

        // set content in main content area
        document.getElementById('content-title').innerHTML = title;            
        document.getElementById('beer-description').innerHTML = description;            
        document.getElementById('abv-content').innerHTML = abv;            
        document.getElementById('ibu-content').innerHTML = ibuMin;            
        document.getElementById('srm').innerHTML = srm;            
        document.getElementById('og').innerHTML = og;            
        document.getElementById('beer-style').innerHTML = contentStyle;
        
        // display image if available for the beer
        document.getElementById('beerImage').src = imageDetail;
        
        // set range sliders in header
        var valAbv = (100-(abvMax+abvMin));
        var rangeAbv = '<div id="abvSlide" class="slidecontainer"><input type="range" min="1" max="100" value="'+valAbv+'" class="slider" id="abvRange"></div>';
        document.getElementById('rangeSliderAbv').innerHTML = rangeAbv;
        var valIbu = (100-(ibuMax+ibuMin));
        var rangeIbu = '<div id="ibuSlide" class="slidecontainer"><input type="range" min="1" max="100" value="'+valIbu+'" class="slider" id="ibuRange"></div>';
        document.getElementById('rangeSliderIbu').innerHTML = rangeIbu;
    })
    .catch(err => console.log(err));     

}
